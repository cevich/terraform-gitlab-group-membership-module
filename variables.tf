variable "token" {
  type        = string
  description = "Gitlab token"
  sensitive   = true
}

variable "base_url" {
  type        = string
  description = "gitlab api end point"
  default     = "https://gitlab.com/api/v4/"
}

variable "parent_full_path" {
  type        = string
  description = "GitLab parent group full path"
}

variable "group_members" {
  type = list(object({
    username     = string
    access_level = string
  }))
  description = "Group members: (gitlab.com handle, user acess)"
}
